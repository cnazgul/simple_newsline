class ChangeFieldsToArticles < ActiveRecord::Migration
  def up
    remove_column :articles, :rating
    add_column :articles, :rating, :integer, :default => 0
  end

  def down
    remove_column :articles, :rating
    add_column :articles, :rating, :integer
  end
end

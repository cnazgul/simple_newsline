class CreateVoteToArticles < ActiveRecord::Migration
  def change
    create_table :vote_to_articles do |t|
      t.integer :user_id
      t.integer :article_id

      t.timestamps
    end
  end
end

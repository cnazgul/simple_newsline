class ArticlesController < ApplicationController
  load_and_authorize_resource :except => [:index, :show]
  before_filter :article_find, :only => [:show, :edit, :update, :destroy, :rating_articles, :its_article?]
  before_filter :its_article?, :only => [:edit, :update, :destroy]
  # GET /articles
  # GET /articles.json
  def index
    if params[:type].to_i == 2
      # Сортировка по рейтингу
      @articles_page = Article.paginate(page: params[:page], order: 'rating desc', per_page: 30)
    else
      # Сортировка по дате создания
      @articles_page = Article.paginate(page: params[:page], order: 'created_at desc', per_page: 30)
    end

    respond_to do |format|
      format.js
      format.html # index.html.erb
    end
  end

  # GET /articles/1
  # GET /articles/1.json
  def show

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /articles/new
  # GET /articles/new.json
  def new
    @article = Article.new

    respond_to do |format|
      format.html # new.html.slim
    end
  end

  # GET /articles/1/edit
  def edit

  end

  # POST /articles
  # POST /articles.json
  def create
    @article = current_user.articles.create(params[:article])

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /articles/1
  # PUT /articles/1.json
  def update

    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy

    respond_to do |format|
      format.html { redirect_to articles_url }
    end
  end

  def rating_articles
    @article.vote_and_rating(current_user,params[:rating].to_i)

    respond_to do |format|
      format.js
    end
  end

  def user_articles
    @articles = current_user.articles

    respond_to do |format|
      format.html #{ redirect_to  }
    end
  end

# Возможность удалять и редактировать только свои статьи
  private
  def its_article?
    if @article.user_id != current_user.id
      return redirect_to root_path
    end
  end

  def article_find
    @article = Article.find(params[:id].to_i)
  end

end

class Article < ActiveRecord::Base
  attr_accessible :rating, :text, :title, :user_id

  validate :title, :text, presence: true
  validate :title, length: { in: 10..80 }

  belongs_to :user
  has_many :vote_to_articles

  def vote_and_rating(current_user,params)
    if (params == 1)
      self.rating = self.rating + 1
      self.save
    else
      self.rating = self.rating - 1
      self.save
    end
    vote = self.vote_to_articles.create
    vote.user_id = current_user.id
    vote.save
  end

end

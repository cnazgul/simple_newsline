class VoteToArticle < ActiveRecord::Base
  attr_accessible :article_id, :user_id

  belongs_to :article

end

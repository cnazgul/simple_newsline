class User < ActiveRecord::Base
  before_create :setup_role
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :role_ids
  # attr_accessible :title, :body

  has_many :articles
  has_and_belongs_to_many :roles

  def role?(role)
    return !!self.roles.find_by_name(role.to_s)
  end

# Установка роли по id. В конкретном случае использование данного подхода обосновано.
# id:1 - user
  private
  def setup_role
    if self.role_ids.empty?
      self.role_ids = [1]
    end
  end

end

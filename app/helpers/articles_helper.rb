module ArticlesHelper
  def vote_user_to_articles(articles)
    @vote = VoteToArticle.where("article_id=? and user_id=?",articles,current_user)
    if @vote.present?
      false
    else
      true
    end
  end
end
